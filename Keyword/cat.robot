# -*- coding: robot -*-

*** Settings ***
Library  Selenium2Library
Library  Dialogs

*** Variables ***
${HOST}  https://www.google.com
${BROWSER}  chrome
${search_word}  cashalo


*** Keywords ***
Proceed With Login Page
  Open Browser  ${HOST}  ${BROWSER}
  Wait Until Page Contains  Cebuano
  Close All Browsers

Click and google search
  Open Browser  ${HOST}  ${BROWSER}
  Wait Until Page Contains  Cebuano
  Wait Until Element Is Visible  xpath=//*[@id="lst-ib"]
  Input Text  xpath=//*[@id="lst-ib"]  ${search_word}
  Focus  xpath=//*[@id="tsf"]/div[2]/div[3]/center/input[1]
  Click Element  xpath=//*[@id="tsf"]/div[2]/div[3]/center/input[1]
  Close All Browsers

